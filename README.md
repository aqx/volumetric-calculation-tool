# Volumetric Calculation Tool v0.2

*Tool that calculates volumetric differences from DEM data*

**Instructions:**
*  Please clone the repository to a Windows machine
*  Run `Main_Application.exe`
*  Wait for console window to show. GUI window will take a while to load afterwards.